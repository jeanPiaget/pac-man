# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called 
by Pacman agents (in searchAgents.py).
"""


import searchAgents
import sys
import util

# Classe no, define todas informacoes do no
import util
class No:

    id = 0 
    def __init__(self, state, parent, action, pathcost):

        self.state = state
        self.parent = parent
        self.action = action
        self.pathcost = pathcost
        self.id = No.id
        No.id = No.id + 1 # gerar id para o prosimo no
    
    def __eq__(self, other):
        
        return self.id == other.id

    def __str__(self):

        if self.parent != None: 
            idp = self.parent.id
        else: 
            idp = None
        return '[%d: %s %s %s %d' % (self.id, self.state, idp, self.action, self.pathcost)
        
    def path(self):
        
        n = self
        path = []
        while n.parent != None:
            path.append(n.action)
            n = n.parent
        path.reverse()            
        return path




class SearchProblem:
    """
  This class outlines the structure of a search problem, but doesn't implement
  any of the methods (in object-oriented terminology: an abstract class).
  
  You do not need to change anything in this class, ever.
  """
  
    def getStartState(self):
        """
     Returns the start state for the search problem 
     """
        util.raiseNotDefined()
    
    def isGoalState(self, state):
        """
       state: Search state
    
     Returns True if and only if the state is a valid goal state
     """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
       state: Search state
     
     For a given state, this should return a list of triples, 
     (successor, action, stepCost), where 'successor' is a 
     successor to the current state, 'action' is the action
     required to get there, and 'stepCost' is the incremental 
     cost of expanding to that successor
     """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
      actions: A list of actions to take
 
     This method returns the total cost of a particular sequence of actions.  The sequence must
     be composed of legal moves
     """
        util.raiseNotDefined()
           

def tinyMazeSearch(problem):
    """
  Returns a sequence of moves that solves tinyMaze.  For any other
  maze, the sequence of moves will be incorrect, so only use this for tinyMaze
  """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

    
def depthFirstSearch(problem):
    """
    Busca em profundidade
    """
    nos = util.Stack(No(problem.getStartState(), None, None, 0))
    return graphSearch(problem, nos)


def breadthFirstSearch(problem):
    """
  Busca em largura
  """
    nos = util.Queue(No(problem.getStartState(), None, None, 0))
    return graphSearch(problem, nos)  
                 
                    
def depthLimitedSearch(problem, limit=1000):
    """
    Profundidade limitada
    """
    nos = util.Queue(No(problem.getStartState(), None, None, 0))
    expandidos = {}
    parar = False
    
    while True:
        if nos.isEmpty(): 
            if parar: return "parar"          
        n = nos.pop()
        expandidos[n.state] = ['E', n]
        if n.pathcost == limit: parar = True
        else:
            for state, action, cost in problem.getSuccessors(n.state):
                ns = No(state, n, action, n.pathcost + cost)         
                if ns.state not in expandidos.keys() and ns not in nos: 
                    if problem.isGoalState(ns.state): return ns.path() 
                    expandidos[ns.state] = ['F', n]
                    nos.push(ns) 



def iterativeDepeningSearch(problem):
    """
    chama Profundidade limitada aoumentando o limite ate atingir o bjectivo
    """
    encontrado = False
    iteracao = 0

    while (not encontrado):
        resultado = depthLimitedSearch(problem, iteracao)
        if resultado != "parar": return resultado
        iteracao += 1


def nullHeuristic(state, problem=None):
    return 0

def manhattanHeuristic(state, problem):
    return searchAgents.manhattanHeuristic(state, problem)   

                                
def uniformCostSearch(problem):
    """
    custo uniforme
    """
    return aStarSearch(problem, nullHeuristic)


def aStarSearch(problem, heur=manhattanHeuristic, greedy=1):
    """
      heur: heurisitc dada para resolver o problema , padrao manhattanHeuristic
      greedy: bandeira para saber se usarmos o custo.
      
  a estrela."
  """ 
    heuristica = heur(problem.getStartState(), problem)
    nos = util.PriorityQueue(No(problem.getStartState(), None, None, 0), heur)  
    expandidos = {}
  
    while True:
        if nos.isEmpty(): sys.exit('failure')
        n = nos.pop()
        if problem.isGoalState(n.state): return n.path()
        expandidos[n.state] = ['E', n]
        for state, action, cost in problem.getSuccessors(n.state):
            heuristica = heur(state, problem)
            ns = No(state, n, action, n.pathcost + cost)         
            if ns.state not in expandidos.keys(): 
                nos.push(ns, (greedy * ns.pathcost) + heuristica)
                expandidos[ns.state] = ['F', n]
            elif ns.state in expandidos.keys() and ns.pathcost < expandidos.get(ns.state)[1].pathcost:
                nos.push(ns, (greedy * ns.pathcost) + heuristica)
                expandidos[ns.state] = ['E', n]


            
def greedySearch(problem, heur=manhattanHeuristic):
    """"
  Procura gulosa
  """
    return aStarSearch(problem, heur, 0)

def graphSearch(problem, nos):
    """
    procura consoante o tipo de ordenacao(pilha ou fila).
    """
    expandidos = {}
    
    while True:
        if nos.isEmpty(): sys.exit('erro')
        n = nos.pop()
        expandidos[n.state] = ['E', n]
        for state, action, cost in problem.getSuccessors(n.state):
            ns = No(state, n, action, n.pathcost + cost)
            if ns.state not in expandidos.keys() and ns not in nos:
                if problem.isGoalState(ns.state):
                    return ns.path()
                expandidos[ns.state] = ['F', n]
                nos.push(ns)
                

# Abbreviations

dfs = depthFirstSearch
dls = depthLimitedSearch
ids = iterativeDepeningSearch
bfs = breadthFirstSearch
ucs = uniformCostSearch
greedy = greedySearch
astar = aStarSearch
